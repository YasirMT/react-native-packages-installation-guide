#### [Home](../)

## react-native-vector-icons
```sh
npm install react-native-vector-icons
```
### android
##### add to app/build.gradle
apply from: "../../node_modules/react-native-vector-icons/fonts.gradle"

### ios
##### add to podfile
```
pod 'RNVectorIcons', :path => '../node_modules/react-native-vector-icons'
```
```
pod install
```
##### edit info.plist
```
<key>UIAppFonts</key>
<array>
  <string>AntDesign.ttf</string>
  <string>Entypo.ttf</string>
  <string>EvilIcons.ttf</string>
  <string>Feather.ttf</string>
  <string>FontAwesome.ttf</string>
  <string>FontAwesome5_Brands.ttf</string>
  <string>FontAwesome5_Regular.ttf</string>
  <string>FontAwesome5_Solid.ttf</string>
  <string>Foundation.ttf</string>
  <string>Ionicons.ttf</string>
  <string>MaterialIcons.ttf</string>
  <string>MaterialCommunityIcons.ttf</string>
  <string>SimpleLineIcons.ttf</string>
  <string>Octicons.ttf</string>
  <string>Zocial.ttf</string>
</array>
```