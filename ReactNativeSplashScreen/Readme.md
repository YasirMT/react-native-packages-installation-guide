#### [Home](../)

## react-native-splash-screen
```
npm install react-native-splash-screen
```
### android

##### apply following changes in MainActivity.java
```diff
+ import android.os.Bundle;
+ import org.devio.rn.splashscreen.SplashScreen;
import com.reactnativenavigation.NavigationActivity;

public class MainActivity extends NavigationActivity {
+    @Override
+    protected void onCreate(Bundle savedInstanceState) {
+        SplashScreen.show(this);
+        super.onCreate(savedInstanceState);
+    }
}
```
##### In src/main/res/layout
###### create launch_screen.xml
```
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/ThemePrimary"
    android:gravity="center"
    android:orientation="vertical">

    <TextView
        android:id="@+id/textView"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="@string/app_name"
        android:textAlignment="gravity"
        android:textColor="#FFFFFF"
        android:textSize="36sp"
        android:textStyle="bold" />

</LinearLayout>
```
##### In src/main/res/values
###### create colors.xml
```
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="ThemePrimary">#005961</color>
</resources>
```
### ios

##### apply following changes in AppDeligate.m
```
//...
#import "RNSplashScreen.h"  // here

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // ...other code
    [RNSplashScreen show];  // here
    return YES;
}

@end
```
##### add to podfile
```
pod 'react-native-splash-screen', :path => '../node_modules/react-native-splash-screen'
```

### Usage

##### add following code in first loading screen 
```
import SplashScreen from 'react-native-splash-screen'

export default class WelcomePage extends Component {

    componentDidMount() {
        // After having done stuff (such as async tasks) hide the splash screen
        SplashScreen.hide();
    }
}
```