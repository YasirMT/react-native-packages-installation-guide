# React-Native 60.5

```sh
react-native init TestApp
cd TestApp
react-native run-android
react-native run-ios

```
```sh
react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
cd android
.\gradlew clean
.\gradlew bundleRelease 

```

#### [ReactNativeNavigation](./ReactNativeNavigation)
#### [ReactNativeLinearGradient](./ReactNativeLinearGradient)
#### [ReactNativeSplashScreen](./ReactNativeSplashScreen)
#### [ReactNativeVectorIcons](./ReactNativeVectorIcons)